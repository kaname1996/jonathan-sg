﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DataBaseService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [WebGet(UriTemplate = "/listaalumnos", ResponseFormat = WebMessageFormat.Json)]
        Alumnos GetAlumnos();
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Alumno
    {
        [DataMember]
        public string nombre;
        //[DataMember]
        //public string apellidop;
        //[DataMember]
        //public string apellidom;
        //[DataMember]
        //public string carrera;
        //[DataMember]
        //public string NSS;
        //[DataMember]
        //public string RFC;
        //[DataMember]
        //public Int64 matricula;
        [DataMember]
        public string[] calificacion;
        [DataMember]
        public string[] materia;
    }
    [DataContract]
    public class Alumnos
    {
        [DataMember]
        public List<Alumno> alumnoslista { get; set; }
    }
}
