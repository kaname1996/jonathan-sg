﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DataBaseService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public Alumnos GetAlumnos()
        {
            Alumnos alumnos = new Alumnos();
            alumnos.alumnoslista = new List<Alumno>();

            string query = "SELECT alumnos.Nombre as nombre, Calificaciones.Calificacion as calificacion, Materias.nombre as materia " +
                "FROM Calificaciones " +
                "INNER JOIN materias on Materias.IdMaterias = Calificaciones.IdMateria " +
                "INNER JOIN Alumnos on Alumnos.IdAlumno = Calificaciones.IdAlumno";

            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=Universidad;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            sqlConnection.Open();

            DataSet ds = new DataSet();
            SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
            adapter.Fill(ds);

            sqlConnection.Close();

            /*debe mostrar
            nombre,calificaciones(en arreglo) y la materia, 
            que muestre todas las materias y 
            calificacion de la tabla Materias
            */
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Alumno alumno = new Alumno();
                alumno.nombre = ds.Tables[0].Rows[i]["nombre"].ToString();
                if(!alumnos.alumnoslista.Exists(x => x.nombre == ds.Tables[0].Rows[i]["nombre"].ToString()))
                {
                    alumnos.alumnoslista.Add(alumno);
                }
            }

            foreach (Alumno a in alumnos.alumnoslista)
            {
                a.calificacion = new string[ds.Tables[0].Rows.Count];
                a.materia = new string[ds.Tables[0].Rows.Count];

                for (int i = 0 ; i < ds.Tables[0].Rows.Count ; i++)
                    {
                        if (ds.Tables[0].Rows[i]["nombre"].ToString() == a.nombre && a.materia != null && a.calificacion != null)
                        {
                            a.calificacion[i] = ds.Tables[0].Rows[i]["calificacion"].ToString();
                            a.materia[i] = ds.Tables[0].Rows[i]["materia"].ToString();
                        }
                    }
                a.calificacion = a.calificacion.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                a.materia = a.materia.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            }

            return alumnos;

        }


    }
}
