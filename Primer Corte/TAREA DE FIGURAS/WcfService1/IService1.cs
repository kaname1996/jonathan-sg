﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService1
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        [WebGet(UriTemplate = "/suma/?numero1={numero1}&numero2={numero2}", ResponseFormat = WebMessageFormat.Json)]
        Resultado GetSumaTotal(double numero1, double numero2);

        [OperationContract]
        [WebGet(UriTemplate = "/resta/?numero1={numero1}&numero2={numero2}", ResponseFormat = WebMessageFormat.Json)]
        Resultado GetrestaTotal(double numero1, double numero2);

        [OperationContract]
        [WebGet(UriTemplate = "/multiple/?numero1={numero1}&numero2={numero2}&tipo={tipo}", ResponseFormat = WebMessageFormat.Json)]
        Resultadomultiple GetmuultipleTotal(double numero1, double numero2, bool tipo);
        //areas de figuras
        /// 
        [OperationContract]
        [WebGet(UriTemplate = "/circulo/?numero1={numero1}", ResponseFormat = WebMessageFormat.Json)]
        Resultado GetAreaCirculo(double numero1);

        [OperationContract]
        [WebGet(UriTemplate = "/triangulo/?numero1={numero1}&numero2={numero2}", ResponseFormat = WebMessageFormat.Json)]
        Resultado GetAreaTriangulo(double numero1, double numero2);

        [OperationContract]
        [WebGet(UriTemplate = "/rectangulo/?numero1={numero1}&numero2={numero2}", ResponseFormat = WebMessageFormat.Json)]
        Resultado GetAreaRectangulo(double numero1, double numero2);

        [OperationContract]
        [WebGet(UriTemplate = "/Cuadrado/?numero1={numero1}&numero2={numero2}", ResponseFormat = WebMessageFormat.Json)]
        Resultado GetAreaCuadrado(double numero1, double numero2);
    }


    // Utilice un contrato de datos, como se ilustra en el ejemplo siguiente, para agregar tipos compuestos a las operaciones de servicio.
    [DataContract]
    public class Resultado
    {
        [DataMember]
        public double total;
    }

    [DataContract]
    public class Resultadomultiple
    {
        [DataMember]
        public double total1;

        [DataMember]
        public double total2;
    }
}
