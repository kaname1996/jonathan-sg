﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService1
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        public Resultado GetSumaTotal(double numero1, double numero2)
        {
            Resultado resultado = new Resultado();
            resultado.total = numero1 + numero2;

            return resultado;
        }

        public Resultado GetrestaTotal(double numero1, double numero2)
        {
            Resultado resultado = new Resultado();
            resultado.total = numero1 - numero2;

            return resultado;
        }

        public Resultadomultiple GetmuultipleTotal(double numero1, double numero2, bool tipo)
        {
            Resultadomultiple resultadomultiple = new Resultadomultiple();

            if (tipo)
            {
                resultadomultiple.total1 = numero1 + numero2;
                resultadomultiple.total2 = numero1 - numero2;
            }
            else
            {
                resultadomultiple.total1 = numero1 * numero2;
                resultadomultiple.total2 = numero1 / numero2;
            }
            return resultadomultiple;
        }
        //area de circulo, triangulo, rectangulo, cuadrado
        public Resultado GetAreaCirculo(double radio)
        {
            Resultado result = new Resultado();
            result.total = 3.14159 * Math.Pow(radio,2);
            return result;
        }

        public Resultado GetAreaTriangulo(double numero1, double numero2)
        {
            Resultado result = new Resultado();
            result.total = (numero1 * numero2) / 2;
            return result;
        }

        public Resultado GetAreaRectangulo(double numero1, double numero2)
        {
            Resultado result = new Resultado();
            result.total = numero1 * numero2;
            return result;
        }

        public Resultado GetAreaCuadrado(double numero1, double numero2)
        {
            Resultado result = new Resultado();
            result.total = numero1 * numero2;
            return result;
        }
    }
}
