﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DistanceMatrix
{
    class MapsApi
    {
        public string CalcularDireccion(string direccionOrigen, string direccionDestino)
        {
            string infoReturn = string.Empty;
            try
            {
                Uri uri = new Uri(@"https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + direccionOrigen + "&destinations=" + direccionDestino + "&key=AIzaSyAAqdQzKGEVMOH4Yn4P4mlzq9qHvsWWbr0");

                WebRequest webRequest = WebRequest.Create(uri);
                WebResponse response = webRequest.GetResponse();
                StreamReader streamReader = new StreamReader(response.GetResponseStream());
                Row row = new Row();
                string respuesta = streamReader.ReadToEnd();

                var jsonobject = JsonConvert.DeserializeObject<RootObject>(respuesta);

                string destAddress = jsonobject.destination_addresses[0];
                string originAddress = jsonobject.origin_addresses[0];
                string dist = jsonobject.rows[0].elements[0].distance.text;
                string duration = jsonobject.rows[0].elements[0].duration.text;

                infoReturn = "Destino: " + destAddress + "\nOrigen: "+ originAddress + "\nDistancia: " + dist + "\nDuracion: " + duration;
            }
            catch (Exception e)
            {
                Console.WriteLine("error: " + e);
            }

            return infoReturn;


            //clic derecho en solution en el buscador de soluciones
            //manage NuGet
            //newtonsoft.json descargar

            // crear windows form o webapp que reciba dos latitudes y longitudes
            // y que imprima la longitud y duracion entre los 2

            // el api acepta direcciones y regresa longitud latitud
        }

        public class Distance
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        public class Duration
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        public class Element
        {
            public Distance distance { get; set; }
            public Duration duration { get; set; }
            public string status { get; set; }
        }

        public class Row
        {
            public List<Element> elements { get; set; }
        }

        public class RootObject
        {
            public List<string> destination_addresses { get; set; }
            public List<string> origin_addresses { get; set; }
            public List<Row> rows { get; set; }
            public string status { get; set; }
        }
    }
}


