﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistanceMatrix
{
    public partial class Form1 : Form
    {
        MapsApi distMatrix = new MapsApi();
        public Form1()
        {
            InitializeComponent();
            labelResult.Text = ""; 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string origen = textBoxOrigen.Text + ", Mexicali";
                string destino = textBoxDestino.Text + ", Mexicali";
                labelResult.Text = distMatrix.CalcularDireccion(origen, destino);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }
            
        }
    }
}
