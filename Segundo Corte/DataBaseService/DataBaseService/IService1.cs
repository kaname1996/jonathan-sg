﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DataBaseService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [WebGet(UriTemplate = "/listaalumnos",ResponseFormat = WebMessageFormat.Json)]
        Alumnos GetAlumnos();

        [WebInvoke(UriTemplate = "/agregaralumno", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, Method = "POST")]
        Respuesta InsertAlumno(Alumno alumno);

        [WebGet(UriTemplate = "/creartoken", ResponseFormat = WebMessageFormat.Json)]
        Respuesta GetToken();
    }

    [DataContract]
    public class Respuesta
    {
        [DataMember]
        public string estado;
    }


        [DataContract]
    public class Alumno
    {
        [DataMember]
        public string nombre;
        [DataMember]
        public string apellidop;
        [DataMember]
        public string apellidom;
        [DataMember]
        public string carrera;
        [DataMember]
        public string NSS;
        [DataMember]
        public string RFC;
        [DataMember]
        public int matricula;
    }

    [DataContract]
    public class Alumnos
    {
        [DataMember]
        public List<Alumno> alumnoslista { get; set; }
    }
}
