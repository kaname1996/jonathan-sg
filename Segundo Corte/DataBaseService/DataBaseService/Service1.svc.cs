﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DataBaseService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {

        public Respuesta GetToken()
        {
            Respuesta respuesta = new Respuesta();
            string stringconnection = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=Escuela;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            int ID = 0;
           

            string llave = "UPBC" + DateTime.Now.ToString();
            SHA1 sha1 = SHA1.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha1.ComputeHash(encoding.GetBytes(llave));
            for(int i=0 ; i < stream.Length ; i++)
            {
                sb.AppendFormat("{0:x2}", stream[i]);
            }
            respuesta.estado = sb.ToString();

            //Consulta de ID
            using (SqlConnection conn = new SqlConnection(stringconnection))
            {
                conn.Open();
                string query = "SELECT IDseguridad FROM Seguridad";
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    ID = reader.GetInt32(0);
                }

                reader.Close();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            //actualizar apikey en BD
            using (SqlConnection conn = new SqlConnection(stringconnection))
            {
                SqlCommand cmd = conn.CreateCommand();
                string query = string.Empty;
                conn.Open();
                if(ID == 1)
                {
                    ID += 1;
                    query = "UPDATE Seguridad " +
                   "SET IDseguridad = " + (ID) + "," +
                   "apikey = '" + respuesta.estado + "'," +
                   "TOC = SYSDATETIME()," +
                   "TOU = SYSDATETIME() " +
                   "where idseguridad = " + (ID - 1);
                }
                else
                {
                    ID += 1;
                    query = "UPDATE Seguridad " +
                     "SET IDseguridad = " + ID + "," +
                     "apikey = '" + respuesta.estado + "'," +
                     "TOU = SYSDATETIME() " +
                     "where idseguridad = " + (ID - 1);
                }

                SqlCommand sqlCommand = new SqlCommand(query, conn);
                cmd.CommandText = query;

                cmd.ExecuteNonQuery();
                conn.Close();
            }////////////////

            return respuesta;
        }



        public Respuesta InsertAlumno(Alumno alumno)
        {
            Respuesta respuesta = new Respuesta();
            string stringconnection = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=Universidad;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            using (SqlConnection conn= new SqlConnection(stringconnection))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText= "Insert into Alumnos (Nombre,ApellidoP,ApellidoM,IdCarrera,NSS,RFC, Matricula) "+
                    "values ('"+ alumno.nombre + "','" + alumno.apellidop + "','" + alumno.apellidom + "','"+
                     alumno.carrera + "','" + alumno.NSS + "','" + alumno.RFC + "'," + alumno.matricula + ")";
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            respuesta.estado = "ok";
            return respuesta;
        }

        public Alumnos GetAlumnos()
        {
            IncomingWebRequestContext request = WebOperationContext.Current.IncomingRequest;
            string apikey = request.Headers["apikey"];
            Alumnos alumnos = new Alumnos();

            //if(apikey == "2ab72f165a28f25fd1a5b73650ad8e68f9df0daa")
            //{
            alumnos.alumnoslista = new List<Alumno>();

            string query = "Select A.nombre, a.apellidop, a.apellidom, a.NSS, a.RFC, a.matricula, c.nombre as carrera " +
                "from Alumnos AS A, Carrera AS C " +
                "WHERE c.idcarrera = a.idcarrera";

            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=Universidad;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            sqlConnection.Open();

            DataSet ds = new DataSet();
            SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
            adapter.Fill(ds);

            sqlConnection.Close();

            for (int i=0;i< ds.Tables[0].Rows.Count; i++)
            {
                Alumno alumno = new Alumno();
                alumno.nombre = ds.Tables[0].Rows[i]["nombre"].ToString();
                alumno.apellidop = ds.Tables[0].Rows[i]["apellidop"].ToString();
                alumno.apellidom = ds.Tables[0].Rows[i]["apellidom"].ToString();
                alumno.NSS = ds.Tables[0].Rows[i]["NSS"].ToString();
                alumno.RFC = ds.Tables[0].Rows[i]["RFC"].ToString();
                alumno.carrera = ds.Tables[0].Rows[i]["carrera"].ToString();
                alumno.matricula = Convert.ToInt32(ds.Tables[0].Rows[i]["matricula"]);
                alumnos.alumnoslista.Add(alumno);
            }
            //}

            
            return alumnos;
        }


    }
}
